#-------------------------------------------------
#
# Project created by QtCreator 2017-04-17T21:55:45
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_testtest
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += qtest

TEMPLATE = app


SOURCES += tst_testtest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS +=
