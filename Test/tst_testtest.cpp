#include <QString>
#include <QtTest>
#include "Calculator/mymath.h"

class Test_MyMath : public QObject {
Q_OBJECT
private slots:
    void sum();
    void mult();
    void diff();
    void separ();
    void sum_mem();
    void diff_mem();
    void sum_diff_check();
    void mult_check();
    void sum_string();
    void mult_string();
    void factorial();
};

void Test_MyMath::sum()
{
    MyMath mymath;
    QCOMPARE(mymath.sum(25, 0), 25);
    QCOMPARE(mymath.sum(-12, -5), -17);
    QCOMPARE(mymath.sum(2007, 2007), 4014);
    QCOMPARE(mymath.sum(-12, 5), -7);
}

void Test_MyMath::mult()
{
    MyMath mymath;
    QCOMPARE(mymath.mult(25, 0), 0);
    QCOMPARE(mymath.mult(-12, -5), 60);
    QCOMPARE(mymath.mult(2007, 2007), 4028049);
    QCOMPARE(mymath.mult(-12, 5), -60);
}

void Test_MyMath::diff()
{
    MyMath mymath;
    QCOMPARE(mymath.diff(25, 0), 25);
    QCOMPARE(mymath.diff(12, 14), -2);
    QCOMPARE(mymath.diff(-12, -5), -7);
    QCOMPARE(mymath.diff(82, 4), 78);
}

void Test_MyMath::separ()
{
    MyMath mymath;
    QCOMPARE(mymath.separ(0.0, 25.0),0.0);
    QCOMPARE(mymath.separ(-12.0, -5.0), 2.4);
    QCOMPARE(mymath.separ(2007.0, 2007.0), 1.0);
    QCOMPARE(mymath.separ(-12.0, 5.0), -2.4);
}

void Test_MyMath::sum_mem()
{
    MyMath mymath;
    QCOMPARE(mymath.sum_mem(0, 0), QString(""));
    QCOMPARE(mymath.sum_mem(9, 5), QString("1.."));
    QCOMPARE(mymath.sum_mem(156, 365), QString("11.."));
    QCOMPARE(mymath.sum_mem(1156, 365), QString("11.."));
    QCOMPARE(mymath.sum_mem(1926, 365), QString("1..1.."));
}

void Test_MyMath::diff_mem()
{
    MyMath mymath;
    QCOMPARE(mymath.diff_mem(0, 0), QString(""));
    QCOMPARE(mymath.diff_mem(999, 758), QString(""));
    QCOMPARE(mymath.diff_mem(4312, 901), QString(".      "));
    QCOMPARE(mymath.diff_mem(4322, 104), QString(".  "));
    QCOMPARE(mymath.diff_mem(4322, 504), QString(".   .  "));
    QCOMPARE(mymath.diff_mem(1009, 423), QString(". .    "));
    QCOMPARE(mymath.diff_mem(1347, 468), QString(". . .  "));
    QCOMPARE(mymath.diff_mem(1509, 23), QString(".    "));
	QCOMPARE(mymath.diff_mem(15009, 23), QString(". .    "));
	QCOMPARE(mymath.diff_mem(10009, 423), QString(". . .    "));
	QCOMPARE(mymath.diff_mem(700000, 423), QString(". . . . .  "));
    QCOMPARE(mymath.diff_mem(1230593, 996), QString(". . . .  "));
}

void Test_MyMath::sum_diff_check()
{
    MyMath mymath;
    QCOMPARE(mymath.sum_diff_check(15, 8, 1), QString("15+8=23+"));
    QCOMPARE(mymath.sum_diff_check(15, -8, 1), QString("15+(-8)=15-8=7-"));
    QCOMPARE(mymath.sum_diff_check(-15, 8, 1), QString("-15+8=8-15=-(15-8)=-7-"));
    QCOMPARE(mymath.sum_diff_check(-15, -8, 1), QString("-15+(-8)=-15-8=-(15+8)=-23+"));
    QCOMPARE(mymath.sum_diff_check(16, 20, 1), QString("16+20=20+16=36+"));
    QCOMPARE(mymath.sum_diff_check(16, -20, 1), QString("16+(-20)=16-20=-(20-16)=-4-"));
    QCOMPARE(mymath.sum_diff_check(-16, 20, 1), QString("-16+20=20-16=4-"));
    QCOMPARE(mymath.sum_diff_check(-16, -20, 1), QString("-16+(-20)=-16-20=-20-16=-(20+16)=-36+"));

    QCOMPARE(mymath.sum_diff_check(15, 8, 0), QString("15-8=7-"));
    QCOMPARE(mymath.sum_diff_check(15, -8, 0), QString("15-(-8)=15+8=23+"));
    QCOMPARE(mymath.sum_diff_check(-15, 8, 0), QString("-15-8=-(15+8)=-23+"));
    QCOMPARE(mymath.sum_diff_check(-15, -8, 0), QString("-15-(-8)=-15+8=8-15=-(15-8)=-7-"));
    QCOMPARE(mymath.sum_diff_check(16, 20, 0), QString("16-20=-(20-16)=-4-"));
    QCOMPARE(mymath.sum_diff_check(16, -20, 0), QString("16-(-20)=16+20=20+16=36+"));
    QCOMPARE(mymath.sum_diff_check(-16, 20, 0), QString("-16-20=-20-16=-(20+16)=-36+"));
    QCOMPARE(mymath.sum_diff_check(-16, -20, 0), QString("-16-(-20)=-16+20=20-16=4-"));
}

void Test_MyMath::mult_check()
{
    MyMath mymath;
    QCOMPARE(mymath.mult_check(25, 10), QString("25*10=250"));
    QCOMPARE(mymath.mult_check(-25, 10), QString("(-25)*10=-(25*10)=-250"));
    QCOMPARE(mymath.mult_check(-25, -10), QString("(-25)*(-10)=25*10=250"));
    QCOMPARE(mymath.mult_check(25, -10), QString("25*(-10)=-(25*10)=-250"));
    QCOMPARE(mymath.mult_check(2555, 10), QString("2555*10=25550"));
    QCOMPARE(mymath.mult_check(-2555, 10), QString("(-2555)*10=-(2555*10)=-25550"));
    QCOMPARE(mymath.mult_check(-2555, -10), QString("(-2555)*(-10)=2555*10=25550"));
    QCOMPARE(mymath.mult_check(2555, -10), QString("2555*(-10)=-(2555*10)=-25550"));
    QCOMPARE(mymath.mult_check(25, 1024), QString("25*1024=25600"));
    QCOMPARE(mymath.mult_check(-25, 1024), QString("(-25)*1024=-(25*1024)=-25600"));
    QCOMPARE(mymath.mult_check(-25, -1024), QString("(-25)*(-1024)=25*1024=25600"));
    QCOMPARE(mymath.mult_check(25, -1024), QString("25*(-1024)=-(25*1024)=-25600"));
}

void Test_MyMath::sum_string()
{
    MyMath mymath;
    QCOMPARE(mymath.sum_string("422857792660554352220106420023358440539078667462664674884978",
                               "240218135805270810820069089904787170638753708474665730068544"),
             QString("663075928465825163040175509928145611177832375937330404953522"));
    QCOMPARE(mymath.sum_string("422857792660554352220106420023358440539078667462664674884978",
                               "240218135805270810820069089904"),
             QString("422857792660554352220106420023598658674883938273484743974882"));
    QCOMPARE(mymath.sum_string("42285779266055435222010642",
                               "240218135805270810820069089904787170638753708474665730068544"),
             QString("240218135805270810820069089904787212924532974530100952079186"));
}

void Test_MyMath::mult_string()
{
    MyMath mymath;
    QCOMPARE(mymath.mult_string("254796001140256699872569632217746889525025521200005889779963",
                                "768452300548956322100467657575421323567852114789633225889410"),
             QString("195798573246904759279347722279931363295170541331434881928428700016079363618686798179195531199971453968225962499871891830"));
    QCOMPARE(mymath.mult_string("254796001140256699872569632217746889525",
                                "768452300548956322100467657575421323567852114789633225889410"),
             QString("195798573246904759279347722279931363295150929506577585889378815064050380267521241050130729137430250"));
    QCOMPARE(mymath.mult_string("254796001140256699872569632217746889525025521200005889779963",
                                "76845230054895632210046765757542132356785"),
             QString("19579857324690475927934772227993136329517000249149308310166248278148689614293680707509283286260098955"));
}

void Test_MyMath::factorial()
{
    MyMath mymath;
    QCOMPARE(mymath.factorial("17"),QString("355687428096000"));
    //QCOMPARE(mymath.factorial("50"),QString("30414093201713378043612608166064768844377641568960512000000000000"));
    //QCOMPARE(mymath.factorial("87"),QString("2107757298379527717213600518699389595229783738061356212322972511214654115727593174080683423236414793504734471782400000000000000000000"));
    //QCOMPARE(mymath.factorial("144"),QString("5550293832739304789551054660550388117999982337982762871343070903773209740507907044212761943998894132603029642967578724274573160149321818341878907651093495984407926316593053871805976798524658790357488383743402086236160000000000000000000000000000000000"));
}

//QTEST_APPLESS_MAIN(TestTest)
QTEST_MAIN(Test_MyMath)
#include "tst_testtest.moc"
