#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mymath.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_sum_clicked();

    void on_diff_clicked();

    void on_mult_clicked();

    void on_separ_clicked();

    void on_pushButton_clicked();

    void on_sum_big_clicked();

    void on_pushButton_2_clicked();

    void on_mult_big_clicked();

    void on_mult_big_2_clicked();

private:
    Ui::MainWindow *ui;
    MyMath mth;
};

#endif // MAINWINDOW_H
