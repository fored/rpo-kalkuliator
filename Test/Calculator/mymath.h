#ifndef MYMATH_H
#define MYMATH_H
#include <QString>
#include <QDebug>
#include <QBitArray>

class MyMath
{
public:
    int sum(int n1, int n2)
    {
        return n1 + n2;
    }
    int diff(int n1, int n2)
    {
        return n1 - n2;
    }
    qint64 mult(qint64 n1, qint64 n2)
    {
        return n1 * n2;
    }
    double separ(double n1, double n2)
    {
        return n1/n2;
    }
    QString sum_mem(int n1, int n2)
    {
        QString str1 = QString::number(n1);
        QString str2 = QString::number(n2);
        QString out("..");
        bool isMem = false;
        for (int i = 0; i < (str1.length() < str2.length() ? str1.length() : str2.length()); i++)
        {
            QString cifra1 = str1.at(str1.length() - 1 - i);
            QString cifra2 = str2.at(str2.length() - 1 - i);
            QString cifra3 = out.at(0);
            if(cifra1.toInt() + cifra2.toInt() + cifra3.toInt() > 9)
            {
                out.prepend("1");
                isMem = true;
            }
            else
                out.prepend("..");
        }
        if (out.indexOf("1") > 0)
        {
            out = out.mid(out.indexOf("1"));
        }
        return isMem ? out : "";
    }

    QString diff_mem (int n1, int n2)
    {
        QString str1 = QString::number(n1);
        QString str2 = QString::number(n2);
        while (str2.length() < str1.length())
        {
            str2.prepend("0");
        }
        QString out("  ");
        bool isMem = false;
        for (int i = 0; i < str2.length(); i++)
        {
            QString cifra1 = str1.at(str1.length() - 1 - i);
            QString cifra2 = str2.at(str2.length() - 1 - i);
            QString cifra3 =(out.at(1) == '.'? "-1":"0");
            if(cifra1.toInt() + cifra3.toInt()<  cifra2.toInt())
            {
                out.prepend(" .");
                isMem = true;
            }
            else
                out.prepend("  ");
        }
        if (out.indexOf(".") > 0)
        {
            out = out.mid(out.indexOf("."));
        }
        return isMem ? out : "";
    }

    QString sum_diff_check(int n1, int n2, bool sum)
    {
        QString cifra1=QString::number(abs(n1));
        QString cifra2=QString::number(abs(n2));
        QString cifra_rez;
        if (sum) cifra_rez=QString::number(n1+n2);
        else cifra_rez=QString::number(n1-n2);
        QString rez;
        bool flag=0;
        if (abs(n1)>abs(n2))flag=1;
        if (flag)
        {
            if(n1>=0)
            {
                if (((n2>0)&&sum)||((n2<0)&&!sum))
                {
                    if (!sum) rez=cifra1+"-(-"+cifra2+")=";
                    rez=rez+cifra1+"+"+cifra2+"="+cifra_rez+"+";
                }
                if (((n2<0)&&sum)||((n2>0)&&!sum))
                {
                    if (sum) rez=cifra1+"+(-"+cifra2+")=";
                    rez=rez+cifra1+"-"+cifra2+"="+cifra_rez+"-";
                }
            }
            else
            {
                if (((n2>0)&&sum)||((n2<0)&&!sum))
                {
                    if (sum) rez="-"+cifra1+"+"+cifra2+"=";
                    else rez="-"+cifra1+"-(-"+cifra2+")=-"+cifra1+"+"+cifra2+"=";
                    rez=rez+cifra2+"-"+cifra1+"="+"-("+cifra1+"-"+cifra2+")="+cifra_rez+"-";
                }
                if (((n2<0)&&sum)||((n2>0)&&!sum))
                {
                    if (sum) rez="-"+cifra1+"+(-"+cifra2+")=";
                    rez=rez+"-"+cifra1+"-"+cifra2+"="+"-("+cifra1+"+"+cifra2+")="+cifra_rez+"+";
                }
            }
        }
        else
        {
            if(n1>=0)
            {
                if (((n2>0)&&sum)||((n2<0)&&!sum))
                {
                    if (sum) rez=cifra1+"+"+cifra2+"=";
                    else rez=cifra1+"-(-"+cifra2+")="+cifra1+"+"+cifra2+"=";
                    rez=rez+cifra2+"+"+cifra1+"="+cifra_rez+"+";
                }
                if (((n2<0)&&sum)||((n2>0)&&!sum))
                {
                    if (sum) rez=cifra1+"+(-"+cifra2+")=";
                    rez=rez+cifra1+"-"+cifra2+"=-("+cifra2+"-"+cifra1+")="+cifra_rez+"-";
                }
            }
            else
            {
                if (((n2>0)&&sum)||((n2<0)&&!sum))
                {
                    if (sum) rez="-"+cifra1+"+"+cifra2+"=";
                    else rez="-"+cifra1+"-(-"+cifra2+")="+"-"+cifra1+"+"+cifra2+"=";
                    rez=rez+cifra2+"-"+cifra1+"="+cifra_rez+"-";
                }
                if (((n2<0)&&sum)||((n2>0)&&!sum))
                {
                    if (sum) rez="-"+cifra1+"+(-"+cifra2+")=";
                    rez=rez+"-"+cifra1+"-"+cifra2+"=-"+cifra2+"-"+cifra1+"=-("+cifra2+"+"+cifra1+")="+cifra_rez+"+";
                }
            }
        }

        if (n2==0)
        {
            if (sum) rez="+";
            else rez="-";
        }

     return rez;
    }

    QString mult_check(qint64 n1, qint64 n2)
    {
        QString cifra1=QString::number(abs(n1));
        QString cifra2=QString::number(abs(n2));
        QString cifra_rez=QString::number(mult(n1, n2));
        QString rez;
        if (n1<0)
        {
            if (n2<0) rez="(-"+cifra1+")*(-"+cifra2+")="+cifra1+"*"+cifra2+"="+cifra_rez;
            else rez="(-"+cifra1+")*"+cifra2+"=-("+cifra1+"*"+cifra2+")="+cifra_rez;
        }
        else
        {
            if (n2<0) rez=cifra1+"*(-"+cifra2+")=-("+cifra1+"*"+cifra2+")="+cifra_rez;
            else rez=cifra1+"*"+cifra2+"="+cifra_rez;
        }

     return rez;
    }

    QString sum_string(QString n1, QString n2)
    {
        QString result;
        int size;
        if(n1.length() >= n2.length())
        {
            int k = n1.length() - n2.length();
            for(int i = 0; i < k; i++)
            {
                n2.prepend("0");
            }
            size = n1.length() + 1;
        }
        else
        {
            int k = n2.length() - n1.length();
            for(int i = 0; i < k; i++)
            {
                n1.prepend("0");
            }
            size = n2.length() + 1;
        }
        QBitArray memory_bit(size, false);
        QString single_n1, single_n2;
        int temp = 0;
        for(int i = size - 2; i >= 0; i--)
        {
            single_n1 = n1[i];
            single_n2 = n2[i];
            temp = single_n1.toInt() + single_n2.toInt();
            if(memory_bit[i + 1] == true)
            {
                temp += 1;
            }
            if(temp >= 10)
            {
                memory_bit[i] = true;
                temp %= 10;
            }
            result.prepend(QString::number(temp));
            if(i == 0 && memory_bit[i] == true)
                result.prepend("1");
        }
        return result;
    }

    QString mult_string(QString n1, QString n2)
    {
        QString single_n1, single_n2;
        int mem = 0;
        int temp = 0;
        QString result;
        QString tmp_str;
        for(int j = n2.length() - 1; j >= 0; j--)
        {
            single_n2 = n2[j];
            for(int i = n1.length() - 1; i >= 0; i--)
            {
               single_n1 = n1[i];
               temp = single_n1.toInt() * single_n2.toInt() + mem;
               if(temp >= 10)
               {
                   mem = temp / 10;
                   temp %= 10;
               }
               else
                   mem = 0;

               tmp_str.prepend(QString::number(temp));
               if(i == 0 && mem > 0)
               {
                   tmp_str.prepend(QString::number(mem));
                   mem = 0;
               }
            }
            if (result.isEmpty())
                result = tmp_str;
            else
            {
                for(int x = 0; x < n2.length() - 1 - j; x++)
                {
                    tmp_str.append("0");
                }
                result = sum_string(result, tmp_str);
            }
            tmp_str = "";
        }
        return result;
    }
    QString factorial(QString n)
    {
        if (n == "0")
            return "1";
        else
            return mult_string(n, factorial(QString::number(n.toInt() - 1)));
    }

};

#endif // MYMATH_H
