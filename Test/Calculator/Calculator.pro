#-------------------------------------------------
#
# Project created by QtCreator 2017-04-17T22:53:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calculator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    mymath.h

FORMS    += mainwindow.ui
