#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>

QString sum_diff_sign;
bool sum_diff_check_used=0;
QString sum_diff_check_rez;
std::vector<QLabel*> Label_vector;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->line->hide();
    ui->line_2->hide();
    ui->line_3->hide();
    Label_vector.reserve(7);
    ui->in_text1->setValidator( new QRegExpValidator( QRegExp( "[1-9]{1,1}[0-9]{0,1000}[0-9]{0,1000}" ) ));
    ui->in_text2->setValidator( new QRegExpValidator( QRegExp( "[1-9]{1,1}[0-9]{0,1000}[0-9]{0,1000}" ) ));
    ui->out_text->setReadOnly(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_sum_clicked()
{
    if (!sum_diff_check_used)
    {
        sum_diff_check_rez = mth.sum_diff_check(ui->in1->value(),ui->in2->value(),1);
        sum_diff_sign=sum_diff_check_rez.at(sum_diff_check_rez.length()-1);
        sum_diff_check_rez.chop(1);
        sum_diff_check_used=1;
    }
    if (sum_diff_sign=="+")
    {
        int first_num;
        int sec_num;
        if (abs(ui->in1->value())>abs(ui->in2->value()))
        {
          first_num=abs(ui->in1->value());
          sec_num=abs(ui->in2->value());
        }
        else
        {
         first_num=abs(ui->in2->value());
         sec_num=abs(ui->in1->value());
        }
        ui->page_2->hide();
        ui->page->show();
        ui->line->show();
        ui->znak->setStyleSheet(QString("font-size: %1px").arg(32));
        ui->znak->setText("+");
        ui->out->setValue(mth.sum(ui->in1->value(),ui->in2->value()));
        ui->in_st_1->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(QString::number(first_num)));
        ui->in_st_2->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(QString::number(sec_num)));
        ui->out_st->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(mth.sum(first_num,sec_num)));
        ui->out_st_2 -> setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt; color:#d60000;\">%1</span></p></body></html>").arg(mth.sum_mem(first_num,sec_num)));
        ui->rule_label->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:14pt;\">%1</span></p></body></html>").arg(sum_diff_check_rez));
        sum_diff_check_used=0;
    }
    else on_diff_clicked();
}

void MainWindow::on_diff_clicked()
{
    if (!sum_diff_check_used)
    {
        sum_diff_check_rez = mth.sum_diff_check(ui->in1->value(),ui->in2->value(),0);
        sum_diff_sign=sum_diff_check_rez.at(sum_diff_check_rez.length()-1);
        sum_diff_check_rez.chop(1);
        sum_diff_check_used=1;
    }
    if (sum_diff_sign=="-")
    {
        int first_num;
        int sec_num;
        if (abs(ui->in1->value())>abs(ui->in2->value()))
        {
          first_num=abs(ui->in1->value());
          sec_num=abs(ui->in2->value());
        }
        else
        {
         first_num=abs(ui->in2->value());
         sec_num=abs(ui->in1->value());
        }
        ui->page_2->hide();
        ui->page->show();
        ui->line->show();
        ui->znak->setStyleSheet(QString("font-size: %1px").arg(32));
        ui->znak->setText("-");
        ui->out->setValue(mth.diff(ui->in1->value(),ui->in2->value()));
        ui->in_st_1->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(QString::number(first_num)));
        ui->in_st_2->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(QString::number(sec_num)));
        ui->out_st->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(mth.diff(first_num,sec_num)));
        ui->out_st_2 -> setText(mth.diff_mem(first_num,sec_num));
        ui->rule_label->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:14pt;\">%1</span></p></body></html>").arg(sum_diff_check_rez));
        sum_diff_check_used=0;

    }
    else on_sum_clicked();
}

void MainWindow::on_mult_clicked()
{
   if (!Label_vector.empty())
    {
        for (int i=0;i<Label_vector.size();i++)
        {
            Label_vector[i]->hide();
          ui->verticalLayout_5->removeWidget(Label_vector[i]);
        }
    }
    Label_vector.clear();
    ui->rule_label->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:14pt;\">%1</span></p></body></html>").arg(mth.mult_check(ui->in1->value(),ui->in2->value())));
    ui->page->hide();
    ui->page_2->show();
    ui->line_2->show();
    ui->out->setValue(_abs64(mth.mult(ui->in1->value(),ui->in2->value())));
    ui->znak_2->setStyleSheet(QString("font-size: %1px").arg(32));
    ui->znak_2->setText("*");
    ui->in_st_3->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(abs(ui->in1->value())));
    ui->in_st_4->setText(QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(abs(ui->in2->value())));
    int my_length = (QString::number(abs(ui->in2->value()))).length();
    int cifra=ui->in2->value();
    for (int i=0; i < my_length; i++)
    {
        QString space = "";
        Label_vector.push_back(new QLabel());
        int c=cifra%10;
        cifra/=10;
        for (int p=0; p<i; p++) space.append("_");
        QString val = QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span><span style=\" font-size:20pt; color:#f0f0f0;\">%2</span></p></body></html>").arg(abs(ui->in1->value()) * abs(c)).arg(space);
        //размер написать
        Label_vector[i]->setText(val);
        ui->verticalLayout_5->addWidget(Label_vector[i]);
    }
    ui->znak_3->setStyleSheet(QString("font-size: %1px").arg(32));
    ui->znak_3->setText("+");
    ui->line_3->show();
    ui->out_st_4->setText((QString("<html><head/><body><p align=\"right\"><span style=\" font-size:20pt;\">%1</span></p></body></html>").arg(mth.mult(abs(ui->in2->value()), abs(ui->in1->value())))));
}

void MainWindow::on_separ_clicked()
{
    ui->rule_label->setText("");
    if (ui->page->isHidden())ui->page_2->hide();
    else ui->page->hide();
    if (ui->in2->value()==0)
        QMessageBox::information(NULL,QObject::tr("Упс..."),tr("На ноль делить нельзя!!! "));
    else
        ui->out->setValue(mth.separ(ui->in1->value(),ui->in2->value()));
}

void MainWindow::on_pushButton_clicked()
{
    ui->page_3->hide();
    ui->page_4->show();
    setMinimumWidth(686);
    setMinimumHeight(541);
    setMaximumWidth(686);
    setMaximumHeight(541);
}

void MainWindow::on_sum_big_clicked()
{
    ui->out_text->setPlainText(mth.sum_string(ui->in_text1->text(), ui->in_text2->text()));
}

void MainWindow::on_pushButton_2_clicked()
{
    ui->page_4->hide();
    ui->page_3->show();
    setMinimumWidth(884);
    setMinimumHeight(541);
    setMaximumWidth(884);
    setMaximumHeight(541);
}


void MainWindow::on_mult_big_clicked()
{
    ui->out_text->setPlainText(mth.mult_string(ui->in_text1->text(), ui->in_text2->text()));
}

void MainWindow::on_mult_big_2_clicked()
{
    ui->out_text->setPlainText(mth.factorial(ui->in_factr->text()));
}
